import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import mir_eval

from torchtrain import Metric

import time
import json
import os

import sys

sys.path.append("..")
from tools import plot_mireval


class MIREvalMetric(Metric):
    __name__ = "metrics.mir_eval"

    def __init__(self, **kwargs):
        super(MIREvalMetric, self).__init__(**kwargs)

        # self.timestep = kwargs.get("timestep", 0.010666667)
        self.timestep = kwargs.get("timestep", 0.03125)
        self.event_based = kwargs.get("event_based", True)

    def init(self) -> None:
        self.first_batch = None
        self.metrics_names = [
            "Onset_Precision",
            "Onset_Recall",
            "Onset_F-measure",
            "Precision_no_offset",
            "Recall_no_offset",
            "F-measure_no_offset",
            "Precision",
            "Recall",
            "F-measure",
        ]
        self.tmp = torch.zeros(9)
        self.count = 0

    @staticmethod
    def labels2mir_eval(
        labels: np.ndarray,
        timestep: float,
        included_freq: np.ndarray = None,
        event_triggered: bool = True,
    ):
        if included_freq is None:
            midi2labels = None
        else:
            midi2labels = torch.zeros(128, dtype=int)
            midi2labels[included_freq] = torch.arange(
                0, included_freq.shape[0], dtype=int
            )

        if event_triggered:
            # setup array
            intervals = []
            pitches = []

            prev = 0
            current = 0
            begin = 0
            # process notes
            for i, (note, onset, offset) in enumerate(labels):
                if offset and current != 0:
                    intervals.append([begin * timestep, i * timestep])
                    pitches.append(current)
                    current = 0
                    begin = 0
                if onset and current == 0:
                    begin = i
                    # note on or note switch event
                    if midi2labels is not None:
                        current = 440.0 * 2 ** ((midi2labels[note] - 69) / 12)
                    else:
                        current = 440.0 * 2 ** ((note - 69) / 12)

            intervals = np.array(intervals)
            if intervals.shape[0] == 0:
                intervals = np.zeros((0, 2))
            pitches = np.array(pitches)

            return intervals, pitches
        else:
            # setup array
            intervals = []
            pitches = []

            prev = 0
            current = 0
            begin = 0
            # process notes
            labels = np.insert(labels, labels.shape[0], 0.0)

            for i, note in enumerate(labels):
                if current == 0:
                    if note != prev:
                        current = note
                        begin = i
                else:
                    if note != prev:
                        intervals.append([begin * timestep, i * timestep])
                        if included_freq is not None:
                            freq = 440.0 * 2 ** (
                                float(included_freq[current] - 69.0) / 12
                            )
                        else:
                            freq = 440.0 * 2 ** (float(current - 69.0) / 12)
                        pitches.append(freq)

                        current = note
                        begin = i

                prev = note

            intervals = np.array(intervals)
            if intervals.shape[0] == 0:
                intervals = np.zeros((0, 2))
            pitches = np.array(pitches)

            return intervals, pitches

    @staticmethod
    def labels2mir_eval_fullevent(
        labels: np.ndarray,
        timestep: float,
        included_freq: np.ndarray = None,
        noenvent_token: bool = True,
        clearance: int = 3,
        freq: bool = True,
    ):

        if noenvent_token:
            # setup array
            intervals = []
            pitches = []

            note = 0
            begin = -1
            # process notes
            labels = np.insert(labels, labels.shape[0], 0)
            for i, event in enumerate(labels):
                if event != 0:
                    if begin != -1:
                        if (i - begin) >= clearance:
                            # note on or note switch event
                            if included_freq is not None:
                                if freq:
                                    current = 440.0 * np.power(
                                        2, float(included_freq[note] - 69) / 12
                                    )
                                else:
                                    current = int(included_freq[note] - 2)
                            else:
                                if freq:
                                    current = 440.0 * np.power(
                                        2, float((note - 2) - 69) / 12
                                    )
                                else:
                                    current = int(note - 2)

                            intervals.append([begin * timestep, i * timestep])
                            pitches.append(current)

                    if event > 1:
                        note = event
                        begin = i
                    else:
                        begin = -1

            intervals = np.array(intervals)
            if intervals.shape[0] == 0:
                intervals = np.zeros((0, 2))
            pitches = np.array(pitches)

            return intervals, pitches
        else:
            raise Exception("Not implemented")

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "est" in batch_data
        assert "ref" in batch_data

        est = torch.argmax(batch_data["est"], dim=2).unsqueeze(2)
        ref = batch_data["ref"].unsqueeze(2).long()

        data = []

        for i in range(est.shape[0]):
            if self.event_based:
                est_intervals, est_pitches = self.labels2mir_eval_fullevent(
                    est[i].cpu().numpy(),
                    self.timestep,
                    included_freq=batch_data.get("included_freq", None),
                )
                ref_intervals, ref_pitches = self.labels2mir_eval_fullevent(
                    ref[i].cpu().numpy(),
                    self.timestep,
                    included_freq=batch_data.get("included_freq", None),
                )
            else:
                est_intervals, est_pitches = self.labels2mir_eval(
                    labels=est[i].cpu().numpy(),
                    timestep=self.timestep,
                    included_freq=batch_data.get("included_freq", None),
                    event_triggered=False,
                )
                ref_intervals, ref_pitches = self.labels2mir_eval(
                    labels=ref[i].cpu().numpy(),
                    timestep=self.timestep,
                    included_freq=batch_data.get("included_freq", None),
                    event_triggered=False,
                )
            data.append([ref_intervals, ref_pitches, est_intervals, est_pitches])

            if ref_intervals.shape[0] > 0:
                scores = mir_eval.transcription.evaluate(
                    ref_intervals, ref_pitches, est_intervals, est_pitches
                )
                for i in range(9):
                    name = self.metrics_names[i % 9]
                    self.tmp[i] += scores[name]

                self.count += 1

        if self.first_batch is None:
            self.first_batch = data

    def calculate(self) -> None:
        self.tmp /= self.count
        self.append(self.tmp)

    def summarize(self) -> str:
        metrics = self.get_last().view(9, -1)
        return " ".join(
            [
                f"On: {metrics[2].item()*100:.2f}%",
                f"On&P: {metrics[5].item()*100:.2f}%",
                f"On&P&Off: {metrics[8].item()*100:.2f}%",
                f"Score: {metrics[2].item()*20+metrics[5].item()*60+metrics[8].item()*20:.2f}%",
            ]
        )

    def benchmark(self) -> dict:
        metrics = self.get_last().view(9, -1)
        res = {name: metrics[i] for i, name in enumerate(self.metrics_names)}
        res["score"] = (
            metrics[2].item() * 20 + metrics[5].item() * 60 + metrics[8].item() * 20
        )
        return res

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        import matplotlib.pyplot as plt

        metrics = self.get_last().view(9, -1)
        for i, name in enumerate(self.metrics_names):
            writer.add_scalar(self.name + "_" + name, metrics[i], global_step=step)

        for i, data in enumerate(self.first_batch):
            plt.subplot(211)
            plot_mireval(plt.gca(), data[0], data[1])
            plt.subplot(212)
            plot_mireval(plt.gca(), data[2], data[3])

            writer.add_figure(self.name + str(i), plt.gcf(), global_step=step)
