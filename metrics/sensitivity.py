import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np

from torchtrain import Metric

import os


class SensitivityMetric(Metric):
    __name__ = "metrics.sensitivity"

    def __init__(self, **kwargs):
        super(SensitivityMetric, self).__init__(**kwargs)

        self.num_class = kwargs.get("num_class", 1)

    def init(self) -> None:
        self.tp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.tn = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fn = torch.zeros(self.num_class, dtype=int, device=self.device)

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "est" in batch_data
        assert "ref" in batch_data

        notes = batch_data["est"].shape[-1]

        pred = torch.argmax(batch_data["est"], dim=2).view(-1)
        gt = batch_data["ref"].view(-1).long()

        pred_matrix = torch.zeros_like(
            batch_data["est"].view(-1, notes), dtype=bool
        )

        gt_matrix = pred_matrix.clone()
        index = torch.arange(0, pred.shape[0], dtype=int)

        pred_matrix[index, pred[index]] = True
        gt_matrix[index, gt[index]] = True

        self.tp += (pred_matrix & gt_matrix).sum(dim=0)
        self.tn += ((~pred_matrix) & (~gt_matrix)).sum(dim=0)
        self.fp += (pred_matrix & (~gt_matrix)).sum(dim=0)
        self.fn += ((~pred_matrix) & gt_matrix).sum(dim=0)

    def calculate(self) -> None:
        sensitivity = (self.tp).float() / (self.tp + self.fn).float()
        accuracy = (self.tp + self.tn).float() / (
            self.tp + self.tn + self.fp + self.fn
        ).float()

        sensitivity[sensitivity != sensitivity] = 0
        accuracy[accuracy != accuracy] = 0

        self.append(torch.cat([sensitivity, accuracy]))

    def summarize(self) -> str:
        [sen, acc] = self.get_last().view(2, -1)
        return f"sensitivity: {sen.mean()*100:.2f}±{sen.std()*100:.2f}% accuracy: {acc.mean()*100:.2f}±{acc.std()*100:.2f}%"

    def benchmark(self) -> dict:
        [sen, acc] = self.get_last().view(2, -1)
        return {
            f"sensitivity/mean": sen.mean(),
            f"sensitivity/std": sen.std(),
            f"accuracy/mean": acc.mean(),
            f"accuracy/std": acc.std(),
        }

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        from matplotlib.ticker import FuncFormatter
        import matplotlib.pyplot as plt

        [sen, acc] = self.get_last().view(2, -1)

        sen = sen.clone().detach().cpu()
        acc = acc.clone().detach().cpu()

        fig = plt.gcf()
        ax = plt.gca()

        def percentage(x, pos):
            return "{:.1f}%".format(x * 100)

        formattery = FuncFormatter(percentage)
        ax.yaxis.set_major_formatter(formattery)

        ax.scatter(np.arange(sen.shape[0], dtype=int), sen.numpy(), label="sensitivity")
        ax.scatter(np.arange(acc.shape[0], dtype=int), acc.numpy(), label="accuracy")

        ax.set_xlabel("classes")
        ax.set_ylabel("pourcentage")
        ax.legend()
        writer.add_figure(os.path.join(tag_prefix, self.name), fig, global_step=step)
