import math
import torch
import torch.nn as nn
import torch.nn.functional as F

from torchtrain import DeepModel, Settings, HParams

from .embedding import Embedding


class MonophonicLSTM(DeepModel):
    __scope__ = "model.monophonic_lstm"

    def __init__(self, **kwargs):
        super(MonophonicLSTM, self).__init__(**kwargs)

        with self.scope() as p:
            self.token_dim = p.output_size + 2
            self.z_size = p.z_size
            self.hidden_size = p.hidden_size * 2

            self.encoder_embedding = Embedding(
                embedding_mode=True, conv_net="resnet18", latent_size=self.z_size,
            )
            self.lstm = nn.LSTM(
                input_size=self.z_size,
                hidden_size=p.hidden_size,
                num_layers=p.num_layers,
                bidirectional=True,
            )
            """
            for name, param in self.lstm.named_parameters():
                if "bias" in name:
                    nn.init.constant(param, 0.0)
                elif "weight" in name:
                    nn.init.xavier_normal(param)
            """
            self.decoder_pred = nn.Sequential(
                nn.Dropout(p.dropout), nn.Linear(self.hidden_size, self.token_dim),
            )

    def load_encoder_embedding(self, checkpoint: str):
        data = torch.load(checkpoint, map_location="cpu")
        data["model"]["commit"] = ""
        data["model"]["module_name"] = ""
        self.encoder_embedding.from_dict(data["model"], strict=False)

    def forward(self, x):
        bs, length, _ = x.shape

        x_emb = self.encoder_embedding(x)
        lstm, _ = self.lstm(x_emb)
        lstm = lstm.view(bs * length, self.hidden_size)
        y = self.decoder_pred(lstm)

        return y.view(bs, length, self.token_dim)


with Settings.default.scope(MonophonicLSTM.__scope__) as hparams:
    hparams.z_size = 256

    hparams.hidden_size = 128
    hparams.num_layers = 1
    hparams.dropout = 0.3

    hparams.output_size = 34

    hparams.conv.filter1 = 32
    hparams.conv.filter2 = 64
