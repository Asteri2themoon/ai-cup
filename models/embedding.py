import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision.models as models

from torchtrain import DeepModel, Settings


def get_n_params(model):
    pp = 0
    for p in list(model.parameters()):
        nn = 1
        for s in list(p.size()):
            nn = nn * s
        pp += nn
    return pp


class ConvNet(nn.Module):
    __scope__ = "model.conv_net"

    def __init__(
        self,
        output_size: int,
        time_bins: int = 5,
        freq_bins: int = 229,
        dropout_conv: float = 0.25,
        filter1: int = 32,
        filter2: int = 64,
        dropout_dense: float = 0.5,
        z: int = 256,
        embedding: bool = False,
    ):
        super(ConvNet, self).__init__()

        self.time_bins = time_bins
        self.freq_bins = freq_bins
        self.embedding = embedding

        self.conv = nn.Sequential(
            nn.Conv2d(1, filter1, 3, padding=1),
            nn.BatchNorm2d(filter1),
            nn.ReLU(),
            nn.Dropout2d(dropout_conv),
            nn.Conv2d(filter1, filter1, 3, padding=1),
            nn.BatchNorm2d(filter1),
            nn.ReLU(),
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(dropout_conv),
            nn.Conv2d(filter1, filter2, 3, padding=1),
            nn.BatchNorm2d(filter2),
            nn.ReLU(),
            nn.MaxPool2d((1, 2)),
        )

        self.fc = nn.Sequential(
            nn.Dropout(dropout_dense),
            nn.Linear(freq_bins // 4 * filter2, z),
            nn.Tanh(),
        )

        self.softmax = nn.Linear(z, output_size, bias=False)

        print("ConvNet")
        print("filter1:", filter1)
        print("filter2:", filter2)
        print("z:", z)
        print("num parameters:", get_n_params(self))

    def forward(self, x):
        bs, channels, time_bins, freq_bins = x.shape

        assert freq_bins == self.freq_bins
        assert channels == 1

        conv = self.conv(x)

        dense_output = self.fc(
            conv.transpose_(1, 2).reshape(bs * time_bins, channels, -1)
        )
        output = dense_output.view(bs, time_bins, -1)

        if not self.embedding:
            tmp = self.softmax(output)
            output = tmp.view(bs, time_bins, -1)

        return output


class AvgPoolingFreq(nn.Module):
    def __init__(self):
        super(AvgPoolingFreq, self).__init__()

    def forward(self, x):
        x = F.avg_pool2d(x, (1, x.shape[-1]), stride=None, padding=0)
        return x


class LinearSeq(nn.Module):
    def __init__(self, z: int, n: int):
        super(LinearSeq, self).__init__()

        self.z = z
        self.n = n
        self.linear = nn.Linear(z, n, bias=False)

    def forward(self, x):
        batch, tmp = x.shape
        length = tmp // self.z

        x = x.view(batch, self.z, -1)

        x = torch.transpose(x, 1, 2).reshape(batch * length, self.z)
        y = self.linear(x)

        return y.view(batch, length, self.n)


class Identity(nn.Module):
    def __init__(self, z: int):
        super(Identity, self).__init__()

        self.z = z

    def forward(self, x):
        batch, tmp = x.shape
        length = tmp // self.z

        x = x.view(batch, length, self.z)

        return x


class Embedding(DeepModel):
    __scope__ = "model.embedding"

    def __init__(self, embedding_mode=False, **kwargs):
        super(Embedding, self).__init__(**kwargs)

        with self.scope() as p:
            self.conv_net = p.conv_net

            if p.event_based:
                self.output_size = p.output_size + 2
            else:
                self.output_size = p.output_size + 1
            if self.conv_net == "resnet18":
                self.cnn = models.resnet18()
                self.cnn.conv1 = nn.Conv2d(
                    1, 64, kernel_size=(7, 7), stride=(1, 2), padding=(3, 3), bias=False
                )
                self.cnn.maxpool = nn.MaxPool2d(
                    kernel_size=3, stride=(1, 2), padding=1, dilation=1, ceil_mode=False
                )
                self.cnn.layer2[0].conv1 = nn.Conv2d(
                    64,
                    128,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer2[0].downsample[0] = nn.Conv2d(
                    64, 128, kernel_size=(1, 1), stride=(1, 2), bias=False
                )
                self.cnn.layer3[0].conv1 = nn.Conv2d(
                    128,
                    256,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer3[0].downsample[0] = nn.Conv2d(
                    128, 256, kernel_size=(1, 1), stride=(1, 2), bias=False
                )
                self.cnn.layer4[0].conv1 = nn.Conv2d(
                    256,
                    512,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer4[-1].conv1 = nn.Conv2d(
                    512,
                    p.latent_size,
                    kernel_size=(3, 3),
                    stride=(1, 1),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer4[0].downsample[0] = nn.Conv2d(
                    256, p.latent_size, kernel_size=(1, 1), stride=(1, 2), bias=False,
                )
                self.cnn.avgpool = AvgPoolingFreq()
                if embedding_mode:
                    self.cnn.fc = Identity(p.latent_size)
                else:
                    self.cnn.fc = LinearSeq(p.latent_size, self.output_size)
            elif self.conv_net == "resnet34":
                self.cnn = models.resnet34()
                self.cnn.conv1 = nn.Conv2d(
                    1, 64, kernel_size=(7, 7), stride=(1, 2), padding=(3, 3), bias=False
                )
                self.cnn.maxpool = nn.MaxPool2d(
                    kernel_size=3, stride=(1, 2), padding=1, dilation=1, ceil_mode=False
                )
                self.cnn.layer2[0].conv1 = nn.Conv2d(
                    64,
                    128,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer2[0].downsample[0] = nn.Conv2d(
                    64, 128, kernel_size=(1, 1), stride=(1, 2), bias=False
                )
                self.cnn.layer3[0].conv1 = nn.Conv2d(
                    128,
                    256,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer3[0].downsample[0] = nn.Conv2d(
                    128, 256, kernel_size=(1, 1), stride=(1, 2), bias=False
                )
                self.cnn.layer4[0].conv1 = nn.Conv2d(
                    256,
                    512,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer4[0].downsample[0] = nn.Conv2d(
                    256, p.latent_size, kernel_size=(1, 1), stride=(1, 2), bias=False,
                )
                self.cnn.layer4[-1].conv1 = nn.Conv2d(
                    512,
                    p.latent_size,
                    kernel_size=(3, 3),
                    stride=(1, 1),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.avgpool = AvgPoolingFreq()
                if embedding_mode:
                    self.cnn.fc = Identity(p.latent_size)
                else:
                    self.cnn.fc = LinearSeq(p.latent_size, self.output_size)
            elif self.conv_net == "resnet50":
                self.cnn = models.resnet50()
                # print(self.cnn)
                # exit(0)
                self.cnn.conv1 = nn.Conv2d(
                    1, 64, kernel_size=(7, 7), stride=(1, 2), padding=(3, 3), bias=False
                )
                self.cnn.maxpool = nn.MaxPool2d(
                    kernel_size=3, stride=(1, 2), padding=1, dilation=1, ceil_mode=False
                )
                self.cnn.layer2[0].conv2 = nn.Conv2d(
                    128,
                    128,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer2[0].downsample[0] = nn.Conv2d(
                    256, 512, kernel_size=(1, 1), stride=(1, 2), bias=False
                )
                self.cnn.layer3[0].conv2 = nn.Conv2d(
                    256,
                    256,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer3[0].downsample[0] = nn.Conv2d(
                    512, 1024, kernel_size=(1, 1), stride=(1, 2), bias=False
                )
                self.cnn.layer4[0].conv2 = nn.Conv2d(
                    512,
                    512,
                    kernel_size=(3, 3),
                    stride=(1, 2),
                    padding=(1, 1),
                    bias=False,
                )
                self.cnn.layer4[0].downsample[0] = nn.Conv2d(
                    1024, 2048, kernel_size=(1, 1), stride=(1, 2), bias=False,
                )
                self.cnn.avgpool = AvgPoolingFreq()
                if embedding_mode:
                    self.cnn.fc = Identity(2048)
                else:
                    self.cnn.fc = LinearSeq(2048, self.output_size)
            elif self.conv_net == "onset_and_frame":
                self.cnn = ConvNet(
                    self.output_size,
                    time_bins=p.onset_and_frame.time_bins,
                    dropout_conv=p.onset_and_frame.conv.dropout,
                    filter1=p.onset_and_frame.conv.filter1,
                    filter2=p.onset_and_frame.conv.filter2,
                    dropout_dense=p.onset_and_frame.dense.dropout,
                    z=p.latent_size,
                    embedding=embedding_mode,
                )
            else:
                raise Exception(f"unkown embedding type ({self.conv_net})")

    def set_trainable(self, conv: bool = True, classifier: bool = True):
        for param in self.cnn.parameters():
            param.requires_grad = conv

        if self.conv_net == "resnet18":
            try:
                self.cnn.fc.linear.weight.requires_grad = classifier
            except:
                pass
        elif self.conv_net == "onset_and_frame":
            self.cnn.softmax.weight.requires_grad = classifier

    def forward(self, x):
        x = x.unsqueeze(1)
        y = self.cnn(x)

        return y


with Settings.default.scope(Embedding.__scope__) as hparams:
    hparams.conv_net = "onset_and_frame"

    hparams.output_size = 34
    hparams.latent_size = 256

    hparams.event_based = True

    hparams.onset_and_frame.time_bins = 5
    hparams.onset_and_frame.conv.dropout = 0.25
    hparams.onset_and_frame.conv.filter1 = 32
    hparams.onset_and_frame.conv.filter2 = 64
    hparams.onset_and_frame.dense.dropout = 0.5
