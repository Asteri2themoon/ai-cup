import math
import torch
import torch.nn as nn
import torch.nn.functional as F

from torchtrain import DeepModel, Settings

from .embedding import Embedding


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer("pe", pe)

    def forward(self, x):
        x = x + self.pe[: x.size(0), :]
        return self.dropout(x)


class MonophonicTranscription(DeepModel):
    __scope__ = "model.monophonic_transcription"

    def __init__(self, **kwargs):
        super(MonophonicTranscription, self).__init__(**kwargs)

        with self.scope() as p:
            if p.event_based:
                self.token_dim = p.output_size + 2
            else:
                self.token_dim = p.output_size + 1
            self.d_model = p.d_model

        with hparams.scope("model.embedding") as p:
            self.encoder_embedding = Embedding(
                embedding_mode=True, conv_net="resnet18", latent_size=self.d_model
            )

        with self.scope() as p:
            self.pos_encoder = PositionalEncoding(p.d_model, p.dropout_embedding)
            self.transformer = nn.Transformer(
                d_model=p.d_model,
                nhead=p.nhead,
                num_encoder_layers=p.num_encoder_layers,
                num_decoder_layers=p.num_decoder_layers,
                dim_feedforward=p.dim_feedforward,
                dropout=p.dropout,
            )
            self.decoder_embedding = nn.Linear(self.token_dim, p.d_model)
            self.decoder_pred = nn.Linear(p.d_model, self.token_dim)

    def load_encoder_embedding(self, checkpoint: str):
        data = torch.load(checkpoint, map_location="cpu")
        data["model"]["commit"] = ""
        data["model"]["module_name"] = ""
        self.encoder_embedding.from_dict(data["model"], strict=False)

    def forward(self, x, y=None):
        bs, length, _ = x.shape

        mask = self.transformer.generate_square_subsequent_mask(bs).to(x.device)

        x_emb = self.encoder_embedding(x)
        x_pos_emb = self.pos_encoder(x_emb.view(bs, length, self.d_model))

        if y is None:  # greedy decoding
            across_batch = torch.arange(0, bs, dtype=int)
            y = x.new(bs, length, self.token_dim).zero_()

            y_emb = self.decoder_embedding(y)
            y_pos_emb = self.pos_encoder(y_emb)

            decoded = self.transformer(x_pos_emb, y_pos_emb, tgt_mask=mask)

            out = torch.argmax(self.decoder_pred(decoded)[:, 0], dim=1)
            y[across_batch, 0, out[across_batch]] = 1  # update init token

            for i in range(length - 1):
                y_emb = self.decoder_embedding(y)
                y_pos_emb = self.pos_encoder(y_emb)

                decoded = self.transformer(x_pos_emb, y_pos_emb, tgt_mask=mask)

                out = torch.argmax(self.decoder_pred(decoded)[:, i], dim=1)
                y[across_batch, i + 1, out[across_batch]] = 1
        else:  # teacher forcing
            y_emb = self.decoder_embedding(y)
            y_pos_emb = self.pos_encoder(y_emb)

            decoded = self.transformer(x_pos_emb, y_pos_emb, tgt_mask=mask)

            y = self.decoder_pred(decoded)

        return y


with Settings.default.scope(MonophonicTranscription.__scope__) as hparams:
    hparams.d_model = 512
    hparams.nhead = 8
    hparams.num_encoder_layers = 6
    hparams.num_decoder_layers = 6
    hparams.dim_feedforward = 2048
    hparams.dropout = 0.2
    hparams.dropout_embedding = 0.2
    hparams.output_size = 34
    hparams.event_based = True
