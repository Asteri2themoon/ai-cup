import youtube_dl
import librosa
import soundfile as sf
import pretty_midi
import numpy as np

import multiprocessing as mp
import glob
import zipfile
import os

dataset_path = "dataset"
zip_files = ["AIcup_testset_ok.zip"]  # "MIR-ST500_20200521.zip", "AIcup_first.zip"]

sample_rate = 16000
workers = 16


def get_urls(path: str):
    url_files = glob.glob(os.path.join(path, "*/*_link.txt"))
    if len(url_files) == 0:
        url_files = glob.glob(os.path.join(path, "*/*.txt"))

    urls = {}
    for url_file in url_files:
        with open(url_file, "r") as url:
            try:
                url_id = int(os.path.splitext(os.path.split(url_file)[-1])[0])
            except ValueError:
                url_id = int(os.path.split(url_file)[-1].split("_")[0])
            urls[url_id] = url.read()
    return urls


def get_ids(path: str):
    gt_files = glob.glob(os.path.join(path, "*/*_groundtruth.txt"))
    return [int(os.path.split(f)[-1].split("_")[0]) for f in gt_files]


def download_wav(url: str, output: str) -> str:
    ydl_opts = {"outtmpl": output, "format": "bestaudio/best"}
    # "postprocessors": [{"key": "FFmpegExtractAudio", "preferredcodec": "wav"}],
    try:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            infos = ydl.extract_info(url, download=True)
            ydl.prepare_filename(infos)
    except youtube_dl.DownloadError:
        return False
    else:
        return True


def ground_truth2midi(input_file: str, output_file: str):
    midi = pretty_midi.PrettyMIDI()

    singer = pretty_midi.Instrument(program=0)

    with open(input_file, "r") as ground_truth:
        lines = ground_truth.readlines()
        for line in lines:
            begin, end, pitch = line.split(" ")
            note = pretty_midi.Note(
                velocity=100, pitch=int(pitch), start=float(begin), end=float(end)
            )
            singer.notes.append(note)

    midi.instruments.append(singer)
    midi.write(output_file)


def worker_wav(args: str):
    base_dir, music_id, url = args

    # download
    wav_filename = os.path.join(base_dir, str(music_id), f"{music_id}.wav")
    success = download_wav(url, wav_filename)

    # resample
    if success:
        y, _ = librosa.core.load(
            wav_filename, sr=sample_rate, mono=True, dtype=np.int16
        )
        sf.write(wav_filename, y, sample_rate)

    return success


def download_all(base_dir: str, urls: list, workers: int = 8):
    with mp.Pool(workers) as p:
        p.map(worker_wav, zip([base_dir] * len(urls), urls.keys(), urls.values()))


def worker_midi(args: list):
    base_dir, music_id = args

    print(f"converting ground truth file id {music_id}")
    input_file = os.path.join(base_dir, str(music_id), f"{music_id}_groundtruth.txt")
    output_file = os.path.join(base_dir, str(music_id), f"{music_id}.mid")
    ground_truth2midi(input_file, output_file)


def convert_all(base_dir: str, files: list, workers: int = 8):
    with mp.Pool(workers) as p:
        p.map(worker_midi, zip([base_dir] * len(files), files))


print("unzip")
for zip_file in zip_files:
    to_dir = os.path.join(dataset_path, os.path.splitext(zip_file)[0])
    try:
        os.mkdir(to_dir)
    except OSError:
        pass
    with zipfile.ZipFile(os.path.join(dataset_path, zip_file), "r") as zip_ref:
        zip_ref.extractall(to_dir)

print("convert to midi")
for zip_file in zip_files:
    to_dir = os.path.join(dataset_path, os.path.splitext(zip_file)[0])
    base_dir = glob.glob(os.path.join(to_dir, "*"))[0]

    ids = get_ids(base_dir)
    print(f"count of file (to be converted): {len(ids)}")
    convert_all(base_dir, ids, workers=workers)

print("download wav")
for zip_file in zip_files:
    to_dir = os.path.join(dataset_path, os.path.splitext(zip_file)[0])
    base_dir = glob.glob(os.path.join(to_dir, "*"))[0]

    urls = get_urls(base_dir)
    print(f"count of file (to be downloaded): {len(urls)}")
    download_all(base_dir, urls, workers=workers)
