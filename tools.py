import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pretty_midi
import numpy as np


def load_midi(midi: str):
    # load midi
    midi_data = pretty_midi.PrettyMIDI(midi)

    # only one channel for the voice
    voice = midi_data.instruments[0]

    # setup array
    intervals = np.zeros((len(voice.notes), 2))
    pitches = np.zeros(len(voice.notes))

    # process notes
    for i, note in enumerate(voice.notes):
        intervals[i, 0] = note.start
        intervals[i, 1] = note.end
        pitches[i] = 440.0 * 2 ** ((note.pitch - 69) / 12)

    return intervals, pitches


def freq2pitch(f):
    return 12 * (np.log(f / 440.0) / np.log(2)) + 69


def pitch2freq(pitch):
    return 440.0 * np.power(2, ((pitch - 69) / 12))


def load_groundtruth(groundtruth: str):
    # load groundtruth
    with open(groundtruth, "r") as gt_file:
        gt = np.array([line.split(" ") for line in gt_file.readlines()])

    intervals = gt[:, :2].astype(float)
    pitches = pitch2freq(gt[:, 2].astype(int))

    return intervals, pitches


def plot_mireval(ax, ref_intervals, ref_pitches, color=None):
    if color is None:
        color_wheel = plt.rcParams["axes.prop_cycle"].by_key()["color"]
        if len(ax.lines) > 0:
            prev_color = color_wheel.index(ax.lines[-1].get_color())
        else:
            prev_color = len(color_wheel) - 1
        color = color_wheel[(prev_color + 1) % len(color_wheel)]

    for (begin, end), freq in zip(ref_intervals, ref_pitches):
        pitch = freq2pitch(freq).round()
        ax.add_patch(
            patches.Rectangle(
                (begin, pitch - 0.5),
                end - begin,
                1,
                linewidth=1,
                edgecolor="none",
                facecolor=color,
            )
        )
        ax.plot([begin, end], [pitch, pitch], color=color, alpha=0)
