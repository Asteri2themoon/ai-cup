import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F

import matplotlib

matplotlib.use("Agg")

from torchtrain import TrainingBatch, Settings, HParams

from metrics import LossMetric, SensitivityMetric, MIREvalMetric
from models import Embedding, MonophonicTranscription, MonophonicLSTM
from dataset import AICupDataset

import math
import os


class TrainEmbedding(TrainingBatch):
    __scope__ = "training.embedding"

    def __init__(self, **kwargs):
        super(TrainEmbedding, self).__init__(**kwargs)

    def learning_rate_scheduler(self, optimizer, global_step):
        if global_step >= self.phase3_step:
            lr = self.phase3_lr
        elif global_step >= self.phase2_step:
            lr = self.phase2_lr
        else:
            lr = self.phase1_lr

        optimizer.param_groups[0]["lr"] = lr

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size

        with hparams.scope("model.embedding") as p:
            self.model = Embedding(**p.params)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.phase1_lr = p.optim.phase1.lr
            self.phase2_step = p.optim.phase2.step
            self.phase2_lr = p.optim.phase2.lr
            self.phase3_step = p.optim.phase3.step
            self.phase3_lr = p.optim.phase2.lr

            self.optimizer = [
                optim.Adam(self.model.parameters(), weight_decay=p.optim.decay)
            ]

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            pin_memory = False if self.device == "cpu" else True
            loader = AICupDataset(
                base_dir=dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                dropout=p.dataset.dropout,
                gain_max=p.dataset.gain_max,
                crop_freq_max_count=p.dataset.crop_freq_max_count,
                crop_freq_max_size=p.dataset.crop_freq_max_size,
                crop_time_max_count=p.dataset.crop_time_max_count,
                crop_time_max_size=p.dataset.crop_time_max_size,
                pin_memory=pin_memory,
            )
            self.step_per_epoch = len(loader.train)
            self.train_loader = loader.train
            self.validation_loader = loader.validate
            self.test_loader = loader.test
            self.notes = loader.num_class()
            self.included_freq = loader.included_freq

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            SensitivityMetric(num_class=self.notes, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], MIREvalMetric(device=self.device),
        )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler(optimizer[0], batch)

        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        pred = model(spec)

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))

        loss.backward()

        optimizer[0].step()

        res = {"loss": loss}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        pred = model(spec)

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))

        res = {
            "loss": loss,
            "est": pred,
            "ref": labels,
            "included_freq": self.included_freq,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainEmbedding.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.dataset.dropout = 0.3
    hparams.dataset.gain_max = 0.1
    hparams.dataset.crop_freq_max_count = 2
    hparams.dataset.crop_freq_max_size = 50
    hparams.dataset.crop_time_max_count = 15
    hparams.dataset.crop_time_max_size = 20

    hparams.optim.decay = 0.0001
    hparams.optim.phase1.lr = 0.0001
    hparams.optim.phase2.step = 32000
    hparams.optim.phase2.lr = 0.00001
    hparams.optim.phase3.step = 48000
    hparams.optim.phase3.lr = 0.000001


class TrainTransformer(TrainingBatch):
    __scope__ = "training.transformer"

    def __init__(self, **kwargs):
        super(TrainTransformer, self).__init__(**kwargs)

    def learning_rate_scheduler_rsqrt(self, optimizer, global_step):
        warmup = min(global_step / self.warmup_step, 1.0)
        decay = 1 / math.sqrt(max(global_step, self.warmup_step))

        optimizer.param_groups[0]["lr"] = self.lr * self.rsqtr_hidden * warmup * decay

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size

        with hparams.scope("model.monophonic_transcription") as p:
            self.model = MonophonicTranscription(**p.params)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.warmup_step = p.warmup_step
            self.rsqtr_hidden = 1 / math.sqrt(self.model.d_model)
            self.lr = p.lr
            self.clip_grad = p.clip_grad
            self.optimizer = [optim.Adam(self.model.parameters())]

            if len(p.embedding_checkpoint) > 0:
                self.model.load_encoder_embedding(p.embedding_checkpoint)
                self.model.encoder_embedding = self.model.encoder_embedding.to(
                    self.device
                )
                self.model.encoder_embedding.set_trainable(conv=False)

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            # pin_memory = False if self.device == "cpu" else True
            loader = AICupDataset(
                base_dir=dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                dropout=p.dataset.dropout,
                gain_max=p.dataset.gain_max,
                crop_freq_max_count=p.dataset.crop_freq_max_count,
                crop_freq_max_size=p.dataset.crop_freq_max_size,
                crop_time_max_count=p.dataset.crop_time_max_count,
                crop_time_max_size=p.dataset.crop_time_max_size,
                mixed_tracks_max=p.dataset.mixed_tracks_max,
                augmentation_on_supervised=True,
                pin_memory=False,
                event_based=p.event_based,
            )
            self.step_per_epoch = len(loader.train)
            self.train_loader = loader.train
            self.validation_loader = loader.validate
            self.test_loader = loader.test
            self.notes = loader.num_class()
            self.included_freq = loader.included_freq

            self.metrics.add_metrics(
                ["train", "validate", "test"], LossMetric(device=self.device)
            )
            self.metrics.add_metrics(
                ["validate", "test"],
                SensitivityMetric(num_class=self.notes, device=self.device),
            )
            self.metrics.add_metrics(
                ["validate", "test"],
                MIREvalMetric(device=self.device, event_based=p.event_based),
            )

    def labels2token(self, labels):
        bs, length = labels.shape
        output_token = labels.new(bs, length, self.model.token_dim).zero_().float()
        across_batch, across_seq = torch.meshgrid(
            torch.arange(0, bs, dtype=int), torch.arange(0, length, dtype=int)
        )
        output_token[across_batch, across_seq, labels[across_batch, across_seq]] = 1
        return output_token

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler_rsqrt(optimizer[0], batch)

        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        tokens = self.labels2token(labels)

        pred = model(spec, tokens)

        loss = F.cross_entropy(
            pred[:, :-1].reshape(-1, self.notes), labels[:, 1:].reshape(-1)
        )

        loss.backward()

        nn.utils.clip_grad_norm_(model.parameters(), self.clip_grad)

        optimizer[0].step()

        res = {"loss": loss}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        chunk_size = 625
        pieces = int(math.ceil(spec.shape[1] / chunk_size))
        tokens = self.labels2token(labels)
        padding = pieces * chunk_size - spec.shape[1]

        spec = F.pad(spec, (0, 0, 0, padding))
        tokens = F.pad(tokens, (0, 0, 0, padding))
        labels = F.pad(labels, (0, padding))
        spec = spec.view(pieces, chunk_size, -1)
        tokens = tokens.view(pieces, chunk_size, -1)

        nb_batch = int(math.ceil(pieces / self.batch_size))
        preds = []
        for i in range(nb_batch):
            """
            if pieces > (i + 1) * self.batch_size:
                pred = model(
                    spec[i * self.batch_size : (i + 1) * self.batch_size],
                    tokens[i * self.batch_size : (i + 1) * self.batch_size],
                )
            else:
                pred = model(spec[i * self.batch_size :], tokens[i * self.batch_size :])
            """
            if pieces > (i + 1) * self.batch_size:
                pred = model(spec[i * self.batch_size : (i + 1) * self.batch_size],)
            else:
                pred = model(spec[i * self.batch_size :])
            preds.append(pred)
        pred = torch.cat(preds, dim=0)
        """
        preds = []
        for i in range(pieces):
            pred = model(
                spec[
                    :, i * chunk_size : min((i + 1) * chunk_size, spec.shape[1] - 1), :
                ]
            )
            preds.append(pred)
        pred = torch.cat(preds, dim=1)
        labels = labels[:, :-1]
        """

        pred = pred.view(1, -1, self.notes)
        labels = labels.view(1, -1)
        loss = F.cross_entropy(pred.squeeze(), labels.squeeze())

        res = {
            "loss": loss,
            "est": pred,
            "ref": labels,
            "included_freq": self.included_freq,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainTransformer.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.embedding_checkpoint = "checkpoint/embedding.pth"

    hparams.dataset.dropout = 0.0
    hparams.dataset.gain_max = 0.1
    hparams.dataset.crop_freq_max_count = 2
    hparams.dataset.crop_freq_max_size = 50
    hparams.dataset.crop_time_max_count = 3
    hparams.dataset.crop_time_max_size = 100
    hparams.dataset.mixed_tracks_max = 3

    hparams.warmup_step = 16000
    hparams.lr = 1.0
    hparams.event_based = True
    hparams.clip_grad = 1.0
    # hparams.k = 5


class TrainLSTM(TrainingBatch):
    __scope__ = "training.lstm"

    def __init__(self, **kwargs):
        super(TrainLSTM, self).__init__(**kwargs)

    def learning_rate_scheduler(self, optimizer, global_step):
        if global_step >= self.phase3_step:
            lr = self.phase3_lr
        elif global_step >= self.phase2_step:
            lr = self.phase2_lr
        else:
            lr = self.phase1_lr

        optimizer.param_groups[0]["lr"] = lr

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size

        with hparams.scope("model.monophonic_lstm") as p:
            self.model = MonophonicLSTM(**p.params)
            self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.phase1_lr = p.optim.phase1.lr
            self.phase2_step = p.optim.phase2.step
            self.phase2_lr = p.optim.phase2.lr
            self.phase3_step = p.optim.phase3.step
            self.phase3_lr = p.optim.phase2.lr

            self.optimizer = [
                optim.Adam(self.model.parameters(), weight_decay=p.optim.decay)
            ]

        with hparams.scope(self.__scope__) as p:
            if len(p.embedding_checkpoint) > 0:
                self.model.load_encoder_embedding(p.embedding_checkpoint)
                self.model.encoder_embedding = self.model.encoder_embedding.to(
                    self.device
                )

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            loader = AICupDataset(
                base_dir=dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                dropout=p.dataset.dropout,
                gain_max=p.dataset.gain_max,
                crop_freq_max_count=p.dataset.crop_freq_max_count,
                crop_freq_max_size=p.dataset.crop_freq_max_size,
                crop_time_max_count=p.dataset.crop_time_max_count,
                crop_time_max_size=p.dataset.crop_time_max_size,
                pin_memory=False,
            )
            self.step_per_epoch = len(loader.train)
            self.train_loader = loader.train
            self.validation_loader = loader.validate
            self.test_loader = loader.test
            self.notes = loader.num_class()
            self.included_freq = loader.included_freq

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            SensitivityMetric(num_class=self.notes, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], MIREvalMetric(device=self.device),
        )

    def labels2token(self, labels):
        bs, length = labels.shape
        output_token = labels.new(bs, length, self.model.token_dim).zero_().float()
        across_batch, across_seq = torch.meshgrid(
            torch.arange(0, bs, dtype=int), torch.arange(0, length, dtype=int)
        )
        output_token[across_batch, across_seq, labels[across_batch, across_seq]] = 1
        return output_token

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler_rsqrt(optimizer[0], batch)

        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        tokens = self.labels2token(labels)

        pred = model(spec, tokens)

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))
        nn.utils.clip_grad_norm_(model.parameters(), self.clip_grad)

        loss.backward()

        optimizer[0].step()

        res = {"loss": loss}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        pred = model(spec)

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))

        res = {
            "loss": loss,
            "est": pred,
            "ref": labels,
            "included_freq": self.included_freq,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainLSTM.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.embedding_checkpoint = "checkpoint/embedding.pth"

    hparams.dataset.dropout = 0.3
    hparams.dataset.gain_max = 0.1
    hparams.dataset.crop_freq_max_count = 0
    hparams.dataset.crop_freq_max_size = 0
    hparams.dataset.crop_time_max_count = 0
    hparams.dataset.crop_time_max_size = 0

    hparams.optim.decay = 0.0001
    hparams.optim.phase1.lr = 0.0001
    hparams.optim.phase2.step = 32000
    hparams.optim.phase2.lr = 0.00001
    hparams.optim.phase3.step = 48000
    hparams.optim.phase3.lr = 0.000001


class TrainEmbeddingUDA(TrainingBatch):
    __scope__ = "training.embedding_uda"

    def __init__(self, **kwargs):
        super(TrainEmbeddingUDA, self).__init__(**kwargs)

    def learning_rate_scheduler(self, optimizer, global_step):
        if global_step >= self.phase3_step:
            lr = self.phase3_lr
        elif global_step >= self.phase2_step:
            lr = self.phase2_lr
        else:
            lr = self.phase1_lr

        optimizer.param_groups[0]["lr"] = lr

    def tsa_keep(self, global_step, pred, labels):
        if self.tsa == "none":
            return None

        pred_softmax = F.softmax(pred, dim=-1)

        if self.tsa == "linear":
            alpha_t = global_step / self.iter_train_total
        elif self.tsa == "exp":
            alpha_t = torch.exp(
                torch.tensor((global_step / self.iter_train_total - 1) * 5)
            )
        elif self.tsa == "log":
            alpha_t = 1 - torch.exp(
                torch.tensor(-global_step / self.iter_train_total * 5)
            )
        else:
            raise Exception(f"TSA type {self.tsa} unknown")

        K = self.notes
        threshold = (alpha_t * (1.0 - 1.0 / K)) + 1.0 / K

        values, args = torch.max(pred_softmax, dim=-1)
        low_confidence = (values < threshold) | (args != labels)

        return low_confidence

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            checkpoint_transfer = p.checkpoint
            train_conv = p.train_conv

        with hparams.scope("model.embedding") as p:
            self.model = Embedding(**p.params)

            if len(checkpoint_transfer) > 0:
                data = torch.load(checkpoint_transfer, map_location="cpu")
                settings = Settings()
                settings.from_dict(data["model"]["hparams"])
                with settings.scope("model.embedding") as p:
                    embedding = Embedding(**p.params)

                data["model"]["commit"] = ""
                data["model"]["module_name"] = ""
                embedding.from_dict(data["model"], strict=True)

            self.model.set_trainable(conv=train_conv)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.lambda_ = p.lambda_
            self.dataaug_supervised = p.dataaug_supervised
            self.tsa = p.tsa

            self.phase1_lr = p.optim.phase1.lr
            self.phase2_step = p.optim.phase2.step
            self.phase2_lr = p.optim.phase2.lr
            self.phase3_step = p.optim.phase3.step
            self.phase3_lr = p.optim.phase2.lr

            self.optimizer = [
                optim.Adam(self.model.parameters(), weight_decay=p.optim.decay)
            ]

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            pin_memory = False if self.device == "cpu" else True
            loader = AICupDataset(
                base_dir=dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                dropout=p.dataset.dropout,
                gain_max=p.dataset.gain_max,
                crop_freq_max_count=p.dataset.crop_freq_max_count,
                crop_freq_max_size=p.dataset.crop_freq_max_size,
                crop_time_max_count=p.dataset.crop_time_max_count,
                crop_time_max_size=p.dataset.crop_time_max_size,
                mixed_tracks_max=p.dataset.mixed_tracks_max,
                augmentation_on_supervised=self.dataaug_supervised,
                pin_memory=pin_memory,
                event_based=p.event_based,
            )
            self.step_per_epoch = len(loader.train)
            self.train_loader = loader.train
            self.validation_loader = loader.validate
            self.test_loader = loader.test
            self.notes = loader.num_class()
            self.included_freq = loader.included_freq

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            SensitivityMetric(num_class=self.notes, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            MIREvalMetric(device=self.device, event_based=p.event_based),
        )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler(optimizer[0], batch)

        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)
        unlabeled = data.unlabeled.to(self.device)
        augmented = data.augmented.to(self.device)

        pred = model(spec)

        pred_reshape = pred.view(-1, self.notes)
        labels_reshape = labels.view(-1)

        keep = self.tsa_keep(batch, pred_reshape, labels_reshape)

        if keep is None:
            supervised_loss = F.cross_entropy(pred_reshape, labels_reshape)
        elif keep.sum() > 0:
            supervised_loss = F.cross_entropy(
                pred_reshape[keep, :], labels_reshape[keep]
            )
        else:
            supervised_loss = 0

        bs, length, notes = pred.shape
        unlabeled_pred = model(unlabeled)
        unlabeled_pred = (
            F.softmax(unlabeled_pred, dim=-1).detach().view(bs * length, notes)
        )
        augmented_pred = model(augmented)
        augmented_pred = F.softmax(augmented_pred, dim=-1).view(bs * length, notes)
        unsupervised_loss = F.kl_div(
            augmented_pred.log(), unlabeled_pred, reduction="batchmean"
        )

        loss = supervised_loss + self.lambda_ * unsupervised_loss
        loss.backward()

        optimizer[0].step()

        res = {
            "loss": loss,
            "supervised_loss": supervised_loss,
            "unsupervised_loss": unsupervised_loss,
        }

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        pred = model(spec)

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))

        res = {
            "loss": loss,
            "est": pred,
            "ref": labels,
            "included_freq": self.included_freq,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainEmbeddingUDA.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.dataset.dropout = 0.3
    hparams.dataset.gain_max = 0.1
    hparams.dataset.crop_freq_max_count = 2
    hparams.dataset.crop_freq_max_size = 50
    hparams.dataset.crop_time_max_count = 15
    hparams.dataset.crop_time_max_size = 20
    hparams.dataset.mixed_tracks_max = 3

    hparams.lambda_ = 1.0
    hparams.dataaug_supervised = False
    hparams.tsa = "exp"

    hparams.event_based = True

    hparams.train_conv = True
    hparams.checkpoint = ""

    hparams.optim.decay = 0.0001
    hparams.optim.phase1.lr = 0.001
    hparams.optim.phase2.step = 32000
    hparams.optim.phase2.lr = 0.0001
    hparams.optim.phase3.step = 48000
    hparams.optim.phase3.lr = 0.00001


class TrainLSTMUDA(TrainingBatch):
    __scope__ = "training.lstm_uda"

    def __init__(self, **kwargs):
        super(TrainLSTMUDA, self).__init__(**kwargs)

    def learning_rate_scheduler(self, optimizer, global_step):
        if global_step >= self.phase3_step:
            lr = self.phase3_lr
        elif global_step >= self.phase2_step:
            lr = self.phase2_lr
        else:
            lr = self.phase1_lr

        optimizer.param_groups[0]["lr"] = lr

    def tsa_keep(self, global_step, pred, labels):
        if self.tsa == "none":
            return None

        pred_softmax = F.softmax(pred, dim=-1)

        if self.tsa == "linear":
            alpha_t = global_step / self.iter_train_total
        elif self.tsa == "exp":
            alpha_t = torch.exp(
                torch.tensor((global_step / self.iter_train_total - 1) * 5)
            )
        elif self.tsa == "log":
            alpha_t = 1 - torch.exp(
                torch.tensor(-global_step / self.iter_train_total * 5)
            )
        else:
            raise Exception(f"TSA type {self.tsa} unknown")

        K = self.notes
        threshold = (alpha_t * (1.0 - 1.0 / K)) + 1.0 / K

        values, args = torch.max(pred_softmax, dim=-1)
        low_confidence = (values < threshold) | (args != labels)

        return low_confidence

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size

        with hparams.scope("model.monophonic_lstm") as p:
            self.model = MonophonicLSTM(**p.params)

        with hparams.scope(self.__scope__) as p:
            if len(p.embedding_checkpoint) > 0:
                self.model.load_encoder_embedding(p.embedding_checkpoint)
                self.model.encoder_embedding = self.model.encoder_embedding.to(
                    self.device
                )

            self.model.encoder_embedding.set_trainable(conv=p.train_conv)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.lambda_ = p.lambda_
            self.dataaug_supervised = p.dataaug_supervised
            self.tsa = p.tsa

            self.phase1_lr = p.optim.phase1.lr
            self.phase2_step = p.optim.phase2.step
            self.phase2_lr = p.optim.phase2.lr
            self.phase3_step = p.optim.phase3.step
            self.phase3_lr = p.optim.phase2.lr

            self.optimizer = [
                optim.Adam(self.model.parameters(), weight_decay=p.optim.decay)
            ]

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            pin_memory = False if self.device == "cpu" else True
            loader = AICupDataset(
                base_dir=dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                dropout=p.dataset.dropout,
                gain_max=p.dataset.gain_max,
                crop_freq_max_count=p.dataset.crop_freq_max_count,
                crop_freq_max_size=p.dataset.crop_freq_max_size,
                crop_time_max_count=p.dataset.crop_time_max_count,
                crop_time_max_size=p.dataset.crop_time_max_size,
                mixed_tracks_max=p.dataset.mixed_tracks_max,
                augmentation_on_supervised=self.dataaug_supervised,
                pin_memory=pin_memory,
            )
            self.step_per_epoch = len(loader.train)
            self.train_loader = loader.train
            self.validation_loader = loader.validate
            self.test_loader = loader.test
            self.notes = loader.num_class()
            self.included_freq = loader.included_freq

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            SensitivityMetric(num_class=self.notes, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], MIREvalMetric(device=self.device),
        )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler(optimizer[0], batch)

        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)
        unlabeled = data.unlabeled.to(self.device)
        augmented = data.augmented.to(self.device)

        pred = model(spec)

        pred_reshape = pred.view(-1, self.notes)
        labels_reshape = labels.view(-1)

        keep = self.tsa_keep(batch, pred_reshape, labels_reshape)

        if keep is None:
            supervised_loss = F.cross_entropy(pred_reshape, labels_reshape)
        elif keep.sum() > 0:
            supervised_loss = F.cross_entropy(
                pred_reshape[keep, :], labels_reshape[keep]
            )
        else:
            supervised_loss = 0

        bs, length, notes = pred.shape
        unlabeled_pred = model(unlabeled)
        unlabeled_pred = (
            F.softmax(unlabeled_pred, dim=-1).detach().view(bs * length, notes)
        )
        augmented_pred = model(augmented)
        augmented_pred = F.softmax(augmented_pred, dim=-1).view(bs * length, notes)
        unsupervised_loss = F.kl_div(
            augmented_pred.log(), unlabeled_pred, reduction="batchmean"
        )

        loss = supervised_loss + self.lambda_ * unsupervised_loss
        loss.backward()

        optimizer[0].step()

        res = {
            "loss": loss,
            "supervised_loss": supervised_loss,
            "unsupervised_loss": unsupervised_loss,
        }

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        pred = model(spec)

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))

        res = {
            "loss": loss,
            "est": pred,
            "ref": labels,
            "included_freq": self.included_freq,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainLSTMUDA.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.dataset.dropout = 0.3
    hparams.dataset.gain_max = 0.1
    hparams.dataset.crop_freq_max_count = 2
    hparams.dataset.crop_freq_max_size = 50
    hparams.dataset.crop_time_max_count = 15
    hparams.dataset.crop_time_max_size = 20
    hparams.dataset.mixed_tracks_max = 3

    hparams.lambda_ = 1.0
    hparams.dataaug_supervised = False
    hparams.tsa = "exp"

    hparams.train_conv = True
    hparams.embedding_checkpoint = ""

    hparams.optim.decay = 0.0001
    hparams.optim.phase1.lr = 0.001
    hparams.optim.phase2.step = 32000
    hparams.optim.phase2.lr = 0.0001
    hparams.optim.phase3.step = 48000
    hparams.optim.phase3.lr = 0.00001


class TrainTransformerUDA(TrainingBatch):
    __scope__ = "training.transformer_uda"

    def __init__(self, **kwargs):
        super(TrainTransformerUDA, self).__init__(**kwargs)

    def learning_rate_scheduler_rsqrt(self, optimizer, global_step):
        warmup = min(global_step / self.warmup_step, 1.0)
        decay = 1 / math.sqrt(max(global_step, self.warmup_step))

        optimizer.param_groups[0]["lr"] = self.lr * self.rsqtr_hidden * warmup * decay

    def tsa_keep(self, global_step, pred, labels):
        if self.tsa == "none":
            return None

        pred_softmax = F.softmax(pred, dim=-1)

        if self.tsa == "linear":
            alpha_t = global_step / self.iter_train_total
        elif self.tsa == "exp":
            alpha_t = torch.exp(
                torch.tensor((global_step / self.iter_train_total - 1) * 5)
            )
        elif self.tsa == "log":
            alpha_t = 1 - torch.exp(
                torch.tensor(-global_step / self.iter_train_total * 5)
            )
        else:
            raise Exception(f"TSA type {self.tsa} unknown")

        K = self.notes
        threshold = (alpha_t * (1.0 - 1.0 / K)) + 1.0 / K

        values, args = torch.max(pred_softmax, dim=-1)
        low_confidence = (values < threshold) | (args != labels)

        return low_confidence

    def labels2token(self, labels):
        bs, length = labels.shape
        output_token = labels.new(bs, length, self.model.token_dim).zero_().float()
        across_batch, across_seq = torch.meshgrid(
            torch.arange(0, bs, dtype=int), torch.arange(0, length, dtype=int)
        )
        output_token[across_batch, across_seq, labels[across_batch, across_seq]] = 1
        return output_token

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            train_conv = p.train_conv

        with hparams.scope("model.monophonic_transcription") as p:
            self.model = MonophonicTranscription(**p.params)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.warmup_step = p.warmup_step
            self.rsqtr_hidden = 1 / math.sqrt(self.model.d_model)
            self.lr = p.lr
            self.clip_grad = p.clip_grad
            self.optimizer = [optim.Adam(self.model.parameters())]

            if len(p.embedding_checkpoint) > 0:
                self.model.load_encoder_embedding(p.embedding_checkpoint)
                self.model.encoder_embedding = self.model.encoder_embedding.to(
                    self.device
                )
                self.model.encoder_embedding.set_trainable(conv=False)

        with hparams.scope(self.__scope__) as p:
            self.lambda_ = p.lambda_
            self.dataaug_supervised = p.dataaug_supervised
            self.tsa = p.tsa

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            pin_memory = False if self.device == "cpu" else True
            loader = AICupDataset(
                base_dir=dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                dropout=p.dataset.dropout,
                gain_max=p.dataset.gain_max,
                crop_freq_max_count=p.dataset.crop_freq_max_count,
                crop_freq_max_size=p.dataset.crop_freq_max_size,
                crop_time_max_count=p.dataset.crop_time_max_count,
                crop_time_max_size=p.dataset.crop_time_max_size,
                mixed_tracks_max=p.dataset.mixed_tracks_max,
                augmentation_on_supervised=self.dataaug_supervised,
                pin_memory=pin_memory,
                event_based=p.event_based,
            )
            self.step_per_epoch = len(loader.train)
            self.train_loader = loader.train
            self.validation_loader = loader.validate
            self.test_loader = loader.test
            self.notes = loader.num_class()
            self.included_freq = loader.included_freq

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            SensitivityMetric(num_class=self.notes, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], MIREvalMetric(device=self.device),
        )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler_rsqrt(optimizer[0], batch)

        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)
        unlabeled = data.unlabeled.to(self.device)
        augmented = data.augmented.to(self.device)

        tokens = self.labels2token(labels)

        pred = model(spec, tokens)

        pred_reshape = pred.view(-1, self.notes)
        labels_reshape = labels.view(-1)

        keep = self.tsa_keep(batch, pred_reshape, labels_reshape)

        if keep is None:
            supervised_loss = F.cross_entropy(pred_reshape, labels_reshape)
        elif keep.sum() > 0:
            supervised_loss = F.cross_entropy(
                pred_reshape[keep, :], labels_reshape[keep]
            )
        else:
            supervised_loss = 0

        bs, length, notes = pred.shape
        unlabeled_pred = model(unlabeled)
        unlabeled_pred = (
            F.softmax(unlabeled_pred, dim=-1).detach().view(bs * length, notes)
        )
        augmented_pred = model(augmented)
        augmented_pred = F.softmax(augmented_pred, dim=-1).view(bs * length, notes)
        unsupervised_loss = F.kl_div(
            augmented_pred.log(), unlabeled_pred, reduction="batchmean"
        )

        loss = supervised_loss + self.lambda_ * unsupervised_loss
        loss.backward()

        nn.utils.clip_grad_norm_(model.parameters(), self.clip_grad)

        optimizer[0].step()

        res = {
            "loss": loss,
            "supervised_loss": supervised_loss,
            "unsupervised_loss": unsupervised_loss,
        }

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        labels = data.labels.to(self.device)

        chunk_size = 625
        pieces = int(math.ceil(spec.shape[1] / chunk_size))
        preds = []
        for i in range(pieces):
            pred = model(
                spec[
                    :, i * chunk_size : min((i + 1) * chunk_size, spec.shape[1] - 1), :
                ]
            )
            preds.append(pred)
        pred = torch.cat(preds, dim=1)
        labels = labels[:, :-1]

        loss = F.cross_entropy(pred.view(-1, self.notes), labels.view(-1))

        res = {
            "loss": loss,
            "est": pred,
            "ref": labels,
            "included_freq": self.included_freq,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainTransformerUDA.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.lambda_ = 1.0
    hparams.dataaug_supervised = False
    hparams.tsa = "exp"

    hparams.train_conv = True

    hparams.embedding_checkpoint = "checkpoint/embedding.pth"

    hparams.dataset.dropout = 0.0
    hparams.dataset.gain_max = 0.1
    hparams.dataset.crop_freq_max_count = 0
    hparams.dataset.crop_freq_max_size = 0
    hparams.dataset.crop_time_max_count = 0
    hparams.dataset.crop_time_max_size = 0
    hparams.dataset.mixed_tracks_max = 3

    hparams.warmup_step = 16000
    hparams.lr = 1.0
    hparams.clip_grad = 1.0
