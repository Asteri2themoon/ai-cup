import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader, SubsetRandomSampler
import numpy as np

import time
import os
import sys
import glob
import copy
import random


class AICupBatch:
    def __init__(self, data):
        data = list(zip(*data))

        self.spec = torch.stack(data[0], 0)
        self.labels = torch.stack(data[1], 0)

        if data[2][0] is not None:
            self.unlabeled = torch.stack(data[2], 0)
        else:
            self.unlabeled = None

        if data[3][0] is not None:
            self.augmented = torch.stack(data[3], 0)
        else:
            self.augmented = None

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.spec = self.spec.pin_memory()
        self.labels = self.labels.pin_memory()

        if self.unlabeled is not None:
            self.unlabeled = self.unlabeled.pin_memory()
        if self.augmented is not None:
            self.augmented = self.augmented.pin_memory()

        return self

    @staticmethod
    def collate_wrapper(batch):
        return AICupBatch(batch)


class AICupSubset(Dataset):
    def __init__(
        self,
        base_dir: str,
        dropout: float,
        gain_max: float,
        crop_freq_max_count: int,
        crop_freq_max_size: int,
        crop_time_max_count: int,
        crop_time_max_size: int,
        mixed_tracks_max: int,
        event_based: bool = True,
        unlabeled: str = None,
        augmented: str = None,
        augmentation_on_supervised: bool = False,
        max: int = None,
    ):
        self.midi2labels = None

        self.samples = glob.glob(os.path.join(base_dir, "*.npz"))

        if augmented is not None:
            self.samples_augmented = glob.glob(os.path.join(augmented, "*.npz"))
        else:
            self.samples_augmented = None

        if unlabeled is not None:
            self.samples_unlabeled = glob.glob(os.path.join(unlabeled, "*.npz"))
        else:
            self.samples_unlabeled = None

        self.dropout = dropout
        self.gain_max = gain_max
        self.crop_freq_max_count = crop_freq_max_count
        self.crop_freq_max_size = crop_freq_max_size
        self.crop_time_max_count = crop_time_max_count
        self.crop_time_max_size = crop_time_max_size
        self.mixed_tracks_max = mixed_tracks_max
        self.augmentation_on_supervised = augmentation_on_supervised
        self.event_based = event_based

        self.max = max

    def __len__(self):
        if self.max is not None:
            return min(self.max, len(self.samples))
        else:
            return len(self.samples)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.item()

        # get labeled data
        file_name = self.samples[idx]
        data = np.load(file_name)

        spec = torch.from_numpy(data["spectrogram"].astype(np.float32))
        midi = torch.from_numpy(data["labels"]).long()

        if not self.event_based:
            midi = self.event2continuous(midi)

        if midi.shape[0] < spec.shape[0]:
            diff = spec.shape[0] - midi.shape[0]
            midi = torch.cat((midi, torch.zeros(diff, dtype=int)), 0)
        elif midi.shape[0] > spec.shape[0]:
            diff = midi.shape[0] - spec.shape[0]
            spec = torch.cat((spec, torch.zeros((diff, spec.shape[1]))), 0)

        if self.midi2labels is None:
            labels = midi
        else:
            labels = self.midi2labels[midi]

        if self.augmentation_on_supervised:
            spec = self.random_transform_(spec)

        # get random unlabeled data
        if self.samples_unlabeled is not None:
            unlabeled_sample = random.choice(self.samples_unlabeled)

            unlabeled_data = np.load(unlabeled_sample)
            unlabeled_spec = torch.from_numpy(
                unlabeled_data["spectrogram"].astype(np.float32)
            )

            if spec.shape[0] < unlabeled_spec.shape[0]:
                diff = unlabeled_spec.shape[0] - midi.shape[0]
                unlabeled_spec = unlabeled_spec[: spec.shape[0], :]
            elif spec.shape[0] > unlabeled_spec.shape[0]:
                diff = spec.shape[0] - unlabeled_spec.shape[0]
                unlabeled_spec = torch.cat(
                    (unlabeled_spec, torch.zeros((diff, spec.shape[1]))), 0
                )

            augmented_spec = self.random_transform_(unlabeled_spec.clone())

        else:
            unlabeled_spec = None
            augmented_spec = None

        return spec, labels, unlabeled_spec, augmented_spec

    def event2continuous(self, sample):
        prev = 0
        for i, event in enumerate(sample):
            note = event - 1
            if note >= 0:
                prev = note
            sample[i] = prev

        return sample

    def random_transform_(self, sample):
        # random noise
        if self.dropout > 0:
            sample = F.dropout2d(sample, p=self.dropout)

        # random gain
        random_gain = (2 * torch.rand(1) - 1) * self.gain_max
        sample += random_gain

        # mix tracks
        sample = np.exp(sample)
        if self.mixed_tracks_max > 0:
            mixed_count = torch.randint(1, self.mixed_tracks_max, (1,))
            for _ in range(mixed_count):
                track = random.choice(self.samples_augmented)

                track_data = np.load(track)
                track_spec = torch.from_numpy(
                    track_data["spectrogram"].astype(np.float32)
                )

                sample += np.exp(track_spec)

        sample = np.log(sample)

        # crop frequency
        if self.crop_freq_max_count > 0:
            crop_count = torch.randint(1, self.crop_freq_max_count, (1,))
            for _ in range(crop_count):
                freq_range = torch.randint(1, self.crop_freq_max_size, (1,))
                max_freq = sample.shape[-1] - freq_range
                freq = torch.randint(0, max_freq.item(), (1,))

                sample[:, freq : freq + freq_range] = 0

        # crop time
        if self.crop_time_max_count > 0:
            crop_count = torch.randint(1, self.crop_time_max_count, (1,))
            for _ in range(crop_count):
                time_range = torch.randint(1, self.crop_time_max_size, (1,))
                max_time = sample.shape[-2] - time_range
                time = torch.randint(0, max_time.item(), (1,))

                sample[time : time + time_range, :] = 0

        return sample

    def set_filter(
        self, included_freq, mean_onset=0, std_onset=1, mean_offset=0, std_offset=1
    ):
        if included_freq is None:
            self.midi2labels = None
        else:
            self.midi2labels = torch.zeros(128, dtype=int)
            self.midi2labels[included_freq] = torch.arange(
                0, included_freq.shape[0], dtype=int
            )

    def get_dataloader(
        self,
        batch_size: int,
        num_workers: int = 8,
        shuffle: bool = True,
        pin_memory: bool = True,
    ):
        return DataLoader(
            self,
            batch_size=batch_size,
            num_workers=num_workers,
            collate_fn=AICupBatch.collate_wrapper,
            shuffle=shuffle,
            pin_memory=pin_memory,
        )


"""AICup dataset."""


class AICupDataset:
    def __init__(
        self,
        batch_size: int,
        base_dir: str,
        num_workers: int = 8,
        pin_memory: bool = True,
        dropout: float = 0.1,
        gain_max: float = 0.1,
        crop_freq_max_count: int = 2,
        crop_freq_max_size: int = 50,
        crop_time_max_count: int = 15,
        crop_time_max_size: int = 20,
        mixed_tracks_max: int = 3,
        unlabeled: bool = True,
        augmented: bool = True,
        augmentation_on_supervised: bool = False,
        event_based: bool = True,
        **kwargs,
    ):

        if unlabeled:
            unlabeled_path = os.path.join(base_dir, "train", "unlabeled")
        else:
            unlabeled_path = None
        if augmented:
            augdata_path = os.path.join(base_dir, "train", "augmented")
        else:
            augdata_path = None

        self.event_based = event_based

        train = AICupSubset(
            os.path.join(base_dir, "train"),
            dropout=dropout,
            gain_max=gain_max,
            crop_freq_max_count=crop_freq_max_count,
            crop_freq_max_size=crop_freq_max_size,
            crop_time_max_count=crop_time_max_count,
            crop_time_max_size=crop_time_max_size,
            mixed_tracks_max=mixed_tracks_max,
            unlabeled=unlabeled_path,
            augmented=augdata_path,
            augmentation_on_supervised=augmentation_on_supervised,
            event_based=self.event_based,
        )
        validate = AICupSubset(
            os.path.join(base_dir, "validate"),
            dropout=0,
            gain_max=0,
            crop_freq_max_count=0,
            crop_freq_max_size=0,
            crop_time_max_count=0,
            crop_time_max_size=0,
            mixed_tracks_max=0,
            event_based=self.event_based,
        )
        test = AICupSubset(
            os.path.join(base_dir, "test"),
            dropout=0,
            gain_max=0,
            crop_freq_max_count=0,
            crop_freq_max_size=0,
            crop_time_max_count=0,
            crop_time_max_size=0,
            mixed_tracks_max=0,
            event_based=self.event_based,
        )

        if self.included_freq is not None:
            if self.event_based:
                train.set_filter(self.included_freq)
                validate.set_filter(self.included_freq)
                test.set_filter(self.included_freq)
            else:
                included_freq = copy.deepcopy(self.included_freq)
                included_freq[1] = 0
                train.set_filter(included_freq[1:])
                validate.set_filter(included_freq[1:])
                test.set_filter(included_freq[1:])

        self.train = train.get_dataloader(
            batch_size=batch_size, num_workers=num_workers, pin_memory=pin_memory
        )
        self.validate = validate.get_dataloader(
            batch_size=1, num_workers=num_workers, pin_memory=pin_memory
        )
        self.test = test.get_dataloader(
            batch_size=1, num_workers=num_workers, pin_memory=pin_memory
        )

    def num_class(self):
        if self.included_freq is not None:
            if self.event_based:
                return self.included_freq.shape[0]
            else:
                return self.included_freq.shape[0] - 1
        else:
            return 128

    included_freq = torch.tensor(
        [
            0,
            1,
            45,
            46,
            47,
            48,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,
            57,
            58,
            59,
            60,
            61,
            62,
            63,
            64,
            65,
            66,
            67,
            68,
            69,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
        ]
    )
