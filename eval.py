import librosa
import torch
import numpy as np
import mir_eval
import pretty_midi
import soundfile as sf
import matplotlib.pyplot as plt

import argparse
import wave
import multiprocessing as mp
import glob
import os
import copy
import json

from torchtrain import Settings

from dataset import AICupDataset
from torch.utils.data import Dataset, DataLoader
from models import Embedding
from metrics import MIREvalMetric
from preprocess import wav_to_mel, get_info, get_label
from tools import load_midi, plot_mireval, load_groundtruth
from metrics import MIREvalMetric


class AICupEvalBatch:
    def __init__(self, data):
        data = list(zip(*data))

        self.spec = torch.stack(data[0], 0)
        if data[1][0] is not None:
            self.labels = torch.stack(data[1], 0)
        else:
            self.labels = None
        self.id = torch.stack(data[2], 0)

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.spec = self.spec.pin_memory()
        if self.labels is not None:
            self.labels = self.labels.pin_memory()

        return self

    @staticmethod
    def collate_wrapper(batch):
        return AICupEvalBatch(batch)


class AICupEval(Dataset):
    def __init__(self, base_dir: str):
        self.base_dir = base_dir

        file_list = glob.glob(os.path.join(base_dir, "*/*.wav"))
        self.ids = [int(os.path.splitext(os.path.split(f)[-1])[0]) for f in file_list]

        file_list = glob.glob(os.path.join(base_dir, "*/*_link.txt"))
        existing_ids = [os.path.split(f)[-1].split("_")[0] for f in file_list]

        self.transcription = {i: [] for i in existing_ids}

    def __len__(self):
        return len(self.ids)

    def get_transcription_list(self):
        return copy.deepcopy(self.transcription)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.item()

        idx = self.ids[idx]

        # build path and get data
        wave_file = os.path.join(self.base_dir, str(idx), f"{idx}.wav")
        midi_file = os.path.join(self.base_dir, str(idx), f"{idx}.mid")

        # compute spectrogram
        wav, _ = sf.read(wave_file)
        spec = wav_to_mel(wav)

        # load labels
        try:
            labels = get_label(midi_file)
        except:
            labels = None
        else:
            labels = midi2labels[labels]

        spec = torch.from_numpy(spec)

        return spec, labels, torch.tensor(idx)

    def get_dataloader(
        self, batch_size: int, num_workers: int = 8, pin_memory: bool = True,
    ):
        return DataLoader(
            self,
            batch_size=batch_size,
            num_workers=num_workers,
            collate_fn=AICupEvalBatch.collate_wrapper,
            shuffle=False,
            pin_memory=pin_memory,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("directory", help="directory of the dataset")
    parser.add_argument("--checkpoint", "-c", default="checkpoint/embedding.pth")
    parser.add_argument("--output", "-o", default="submission.json")
    parser.add_argument("--device", "-d", default="cuda")
    parser.add_argument("--timestep", "-t", type=float, default=0.032)
    parser.add_argument("--display", "-D", type=bool, default=False)

    args = parser.parse_args()
    print(args)

    device = args.device
    base_dir = args.directory
    checkpoint = args.checkpoint
    display = args.display
    output = args.output
    timestep = args.timestep

    included_freq = torch.tensor(
        [
            0,
            1,
            45,
            46,
            47,
            48,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,
            57,
            58,
            59,
            60,
            61,
            62,
            63,
            64,
            65,
            66,
            67,
            68,
            69,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
        ]
    )

    midi2labels = torch.zeros(128, dtype=int)
    midi2labels[included_freq] = torch.arange(0, included_freq.shape[0], dtype=int)

    # load model
    data = torch.load(checkpoint, map_location="cpu")
    settings = Settings()
    settings.from_dict(data["model"]["hparams"])
    with settings.scope("model.embedding") as p:
        embedding = Embedding(**p.params)

    data["model"]["commit"] = ""
    data["model"]["module_name"] = ""
    embedding.from_dict(data["model"], strict=False)
    embedding = embedding.to(device)

    to_eval = AICupEval(base_dir)

    transcription = to_eval.get_transcription_list()
    aicup_score = 0
    for i, batch in enumerate(to_eval.get_dataloader(batch_size=1, num_workers=16)):
        print(f"proccess {i+1}/{len(to_eval)}")

        current_id = batch.id
        labels = batch.labels
        spec = batch.spec.to(device)

        with torch.no_grad():
            pred = embedding(spec)
        est = torch.argmax(pred, dim=2).unsqueeze(2).detach().cpu().numpy()

        est_intervals, est_pitches = MIREvalMetric.labels2mir_eval_fullevent(
            est[0], timestep=timestep, included_freq=included_freq, freq=False
        )

        midi_estimation = np.concatenate(
            (est_intervals, est_pitches.reshape(-1, 1)), axis=1
        )

        transcription[str(current_id.item())] = [
            [a, b, int(c)] for [a, b, c] in midi_estimation.tolist()
        ]

        if labels is not None:
            ref = labels.unsqueeze(2).long()

            ref_intervals, ref_pitches = MIREvalMetric.labels2mir_eval_fullevent(
                ref[0], timestep=timestep, included_freq=included_freq, freq=False
            )

            if display:
                plt.subplot(211)
                plot_mireval(plt.gca(), ref_intervals, ref_pitches)
                plt.subplot(212)
                plot_mireval(plt.gca(), est_intervals, est_pitches)

            scores = mir_eval.transcription.evaluate(
                ref_intervals, ref_pitches, est_intervals, est_pitches
            )
            print(scores)
            s_tmp = (
                scores["Onset_F-measure"] * 20
                + scores["F-measure_no_offset"] * 60
                + scores["F-measure"] * 20
            )
            aicup_score += s_tmp
            print(f"AI cup score: {s_tmp:.2f}%")
            if display:
                plt.show()

    aicup_score /= len(to_eval)
    print(f"total score: {aicup_score:.2f}%")

    print(f"count: {len(transcription)}")
    with open(output, "w") as fp:
        json.dump(transcription, fp, sort_keys=True, indent=4)
