import librosa
import numpy as np
import pretty_midi
import soundfile as sf

import wave
import multiprocessing as mp
import glob
import os

sample_rate = 16000
timestep = 0.032
spec_hop_length = 512
spec_n_bins = 229
spec_htk = True
spec_log_amp = True
spec_normalize = True
spec_fmin = 30.0
index_offset = 5768

split_validation = 0.0
split_test = 0.0
sequence_length = 20.0
base_dir = "dataset/medleydb-full-annotated"
output_dir = "dataset/preprocess-medleydb-annotated"

workers = 16

assert int(sample_rate * timestep) == spec_hop_length


def wav_to_mel(wav: np.ndarray):
    spec = librosa.feature.melspectrogram(
        wav,
        sample_rate,
        hop_length=spec_hop_length,
        fmin=spec_fmin,
        n_mels=spec_n_bins,
        htk=spec_htk,
    ).astype(np.float32)
    if spec_log_amp:
        spec = librosa.power_to_db(spec)
    if spec_normalize:
        spec = librosa.util.normalize(spec)
    return np.transpose(spec)


def get_ids(path: str):
    gt_files = glob.glob(os.path.join(path, "*/*.wav"))
    return [
        int(os.path.splitext(os.path.split(f)[-1])[0].split("_")[0]) for f in gt_files
    ]


def get_info(base_dir: str, track_id: int):
    wave_file = os.path.join(base_dir, str(track_id), f"{track_id}.wav")
    midi_file = os.path.join(base_dir, str(track_id), f"{track_id}.mid")
    try:
        midi_data = pretty_midi.PrettyMIDI(midi_file)
    except:
        if os.path.exists(wave_file):
            with wave.open(wave_file, "r") as fp:
                frame_rate = fp.getframerate()
                nb_frame = fp.getnframes()
            return nb_frame / frame_rate, wave_file, None
        else:
            return None, None, None
    else:
        if os.path.exists(wave_file):
            return midi_data.get_end_time(), wave_file, midi_file

    return None, None, None


def get_label(midi: str, step: float = timestep) -> np.ndarray:
    # load midi
    midi_data = pretty_midi.PrettyMIDI(midi)

    # create array
    length = np.ceil(midi_data.get_end_time() / step).astype(int)
    labels = np.zeros((length + 2), dtype=int)

    # only one channel for the voice
    voice = midi_data.instruments[0]

    # process notes
    for note in voice.notes:
        start = np.round(note.start / step).astype(int)
        end = np.round(note.end / step).astype(int)
        labels[start] = note.pitch + 2
        labels[start + 1] = note.pitch + 2
        labels[end] = 1
        labels[end + 1] = 1

    return labels


def worker_split(args):
    base_dir, dest_dir, sample_id, split = args

    length, wave_file, midi_file = get_info(base_dir, sample_id)

    if length is None:
        return []

    if split:
        seq_count = np.ceil(length / sequence_length).astype(int)

        seqs = [
            (
                wave_file,
                midi_file,
                sequence_length * i,
                sequence_length * (i + 1),
                dest_dir,
            )
            for i in range(seq_count)
        ]
    else:
        seqs = [(wave_file, midi_file, 0, length, dest_dir)]

    return seqs


def worker_process(args):
    seq_id, (wave_file, midi_file, begin, end, dest_dir) = args
    print(f"proccess {seq_id}")

    # load wav
    i_begin = int(begin * sample_rate)
    i_end = int(end * sample_rate) - 1
    wav, _ = sf.read(wave_file, start=i_begin, stop=i_end)
    # padding
    wav = np.concatenate((wav, np.zeros(i_end - i_begin - wav.shape[0])))

    # compute spectrogram
    spec = wav_to_mel(wav)

    if midi_file is not None:
        # load labels
        i_begin = int(begin * sample_rate // spec_hop_length)
        i_end = int(end * sample_rate // spec_hop_length)
        labels = get_label(midi_file)
        labels = labels[i_begin : min(i_end, labels.shape[0])]
        # padding
        labels = np.concatenate((labels, np.zeros((i_end - i_begin - labels.shape[0]))))
    else:
        labels = np.empty(0)

    try:
        os.makedirs(os.path.join(output_dir, dest_dir))
    except OSError:
        pass

    np.savez(
        os.path.join(output_dir, dest_dir, f"{seq_id+index_offset}.npz"),
        spectrogram=spec,
        labels=labels,
    )


def process_all(
    base_dir: str, ids: list, dest_dir: str, workers: int = 8, split: bool = True
):
    with mp.Pool(workers) as p:
        lst_sequences = p.map(
            worker_split,
            zip([base_dir] * len(ids), [dest_dir] * len(ids), ids, [split] * len(ids)),
        )

    lst_sequences = sum(lst_sequences, [])

    print(f"seq: {len(lst_sequences)}")

    try:
        os.makedirs(output_dir)
    except:
        pass

    with mp.Pool(workers) as p:
        p.map(worker_process, enumerate(lst_sequences))


if __name__ == "__main__":
    ids = get_ids(base_dir)

    count_validation = int(len(ids) * split_validation)
    count_test = int(len(ids) * split_test)

    if split_validation == 1.0:
        set_train = []
        set_validation = ids
        set_test = []
    elif split_test == 1.0:
        set_train = []
        set_validation = []
        set_test = ids
    else:
        if count_test > 0:
            set_train = ids[: -(count_validation + count_test)]
            set_validation = ids[-(count_validation + count_test) : -count_test]
            set_test = ids[-count_test:]
        if count_validation > 0:
            set_train = ids[:-count_validation]
            set_validation = ids[-count_validation:]
            set_test = []
        else:
            set_train = ids
            set_validation = []
            set_test = []
    print(f"Songs in the training set: {len(set_train)}")
    print(f"Songs in the validation set: {len(set_validation)}")
    print(f"Songs in the testing set: {len(set_test)}")
    input("Go?")

    process_all(
        base_dir, set_train, workers=workers, dest_dir="train", split=True,
    )
    process_all(
        base_dir, set_validation, workers=workers, dest_dir="validate", split=False,
    )
    process_all(base_dir, set_test, workers=workers, dest_dir="test", split=False)
