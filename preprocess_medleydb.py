import librosa
import soundfile
import pandas as pd
import numpy as np
import pretty_midi

import os
import os.path as path
import yaml
import glob
import multiprocessing as mp

from tools import plot_mireval, load_midi
from preprocess import sample_rate

dst_path = "dataset/medleydb-full-annotated"
MEDLEYDB_PATH = "/home/arthur/Documents/research/medleydb"

workers = 16

meta_filenames = glob.glob(path.join(MEDLEYDB_PATH, "medleydb/data/Metadata/*.yaml"))
pitch_path = path.join(MEDLEYDB_PATH, "medleydb/data/Annotations/Pitch")

voices = [
    "vocalists",
    "male singer",
    "female singer",
    "male rapper",
    "female rapper",
    "female speaker",
    "male speaker",
    "female screamer",
    "male screamer",
]


def get_stems():
    tracks_instru = {}
    tracks_voices = {}

    for filename in meta_filenames:
        with open(filename, "r") as stream:
            meta = yaml.safe_load(stream)

        for _, stem in meta["stems"].items():
            if "stem_dir" in meta:
                base_path = path.join(MEDLEYDB_PATH, "Audio/*", meta["stem_dir"])
            else:
                base_path = path.join(MEDLEYDB_PATH, "Audio/*")
            audio_path = glob.glob(path.join(base_path, stem["filename"]))

            if len(audio_path) > 0:
                audio_path = audio_path[0]

                if stem["instrument"] in voices:
                    files = tracks_voices.get(stem["instrument"], [])
                    files.append(audio_path)
                    tracks_voices[stem["instrument"]] = files
                else:
                    files = tracks_instru.get(stem["instrument"], [])
                    files.append(audio_path)
                    tracks_instru[stem["instrument"]] = files

    return tracks_instru, tracks_voices


def get_stems_with_annotation():
    tracks = {}

    for filename in meta_filenames:
        with open(filename, "r") as stream:
            meta = yaml.safe_load(stream)

        instru_stems = []
        vocals_stems = []
        for _, stem in meta["stems"].items():
            name = path.splitext(stem["filename"])[0]
            pitch_file = glob.glob(path.join(pitch_path, f"{name}.csv"))

            if "stem_dir" in meta:
                base_path = path.join(MEDLEYDB_PATH, "Audio/*", meta["stem_dir"])
            else:
                base_path = path.join(MEDLEYDB_PATH, "Audio/*")
            audio_path = glob.glob(path.join(base_path, stem["filename"]))

            if len(audio_path) > 0:
                audio_path = audio_path[0]

                if stem["instrument"] in voices and len(pitch_file) > 0:
                    vocals_stems.append((audio_path, pitch_file[0]))
                elif not (stem["instrument"] in voices):
                    instru_stems.append(audio_path)

        if len(vocals_stems) > 0:
            tracks[filename.replace("_METADATA.yaml", "")] = (
                instru_stems,
                vocals_stems,
            )

    return tracks


def generate_stems_combin(tracks):
    mix = []
    for instru_stems, vocals_stems in tracks.values():
        for vocals_stem in vocals_stems:
            mix.append((instru_stems, vocals_stem))
    return mix


def csv2midi_naive(file_name, a4=440.0, min_length=0.025):
    df = pd.read_csv(file_name, header=None, names=list(range(3)))

    df = df[:][df[1].values != 0]
    df = df[[0, 1]]

    step = df[0].diff().min()

    try:
        df[1] = (np.log(df[1] / a4) / np.log(2) * 12 + 69).round()
    except TypeError:
        raise Exception(f"error with {file_name}")
    midi = pretty_midi.PrettyMIDI()

    voice = pretty_midi.Instrument(program=0)

    onset_time = -1
    onset_pitch = -1
    prev_time = 0
    for [_, (time, pitch)] in df.iterrows():
        if pitch != onset_pitch or (time - prev_time) > step * 2:
            if onset_pitch != -1 and (prev_time + step - onset_time) > min_length:
                note = pretty_midi.Note(
                    velocity=100,
                    pitch=onset_pitch,
                    start=onset_time,
                    end=prev_time + step,
                )
                voice.notes.append(note)
            onset_time = time
            onset_pitch = int(pitch)

        prev_time = time

    midi.instruments.append(voice)

    return midi


def csv2midi(file_name, a4=440.0):
    df = pd.read_csv(file_name, header=None)

    # df = df[:][df[1].values != 0]

    df[1] = (np.log(df[1] / a4) / np.log(2) * 12 + 69).round()
    midi = pretty_midi.PrettyMIDI()

    voice = pretty_midi.Instrument(program=0)

    onset_time = -1
    pitchs = {}
    for [_, (time, pitch)] in df.iterrows():
        if pitch != float("-inf"):
            if onset_time == -1:
                onset_time = time
            sum_pitch = pitchs.get(int(pitch), 0) + 1
            pitchs[int(pitch)] = sum_pitch
        elif onset_time != -1:
            index = np.argmax(list(pitchs.values()))
            print(
                index,
                list(pitchs.values()),
                list(pitchs.keys()),
                list(pitchs.keys())[index],
            )
            note = pretty_midi.Note(
                velocity=100,
                pitch=list(pitchs.keys())[index],
                start=onset_time,
                end=time,
            )
            voice.notes.append(note)
            onset_time = -1
            pitchs = {}

    midi.instruments.append(voice)

    midi.write("tmp.mid")

    print(df)

    intervals, pitches = load_midi("tmp.mid")

    import matplotlib.pyplot as plt

    plot_mireval(plt.gca(), intervals, pitches)
    plt.show()

    return None


def blend_stems(track, dst, name):
    instru_stems, (audio_voice, audio_pitch) = track

    midi = csv2midi_naive(audio_pitch)
    midi.write(os.path.join(dst, name + ".mid"))

    wav = []
    for stem in instru_stems:
        data, _ = librosa.load(stem, sr=sample_rate)
        wav.append(data)

    data, _ = librosa.load(audio_voice, sr=sample_rate)
    wav.append(data)

    mixed = wav[0]
    for audio in wav:
        mixed += audio

    mixed = librosa.util.normalize(mixed)

    soundfile.write(os.path.join(dst, name + ".wav"), mixed, sample_rate)


def resample_list(args):
    id_track, (track, dst) = args

    print(f"process {id_track+1}")

    directory = path.join(dst, str(id_track + 1))
    file_dst = path.join(directory, f"{id_track+1}.wav")

    try:
        os.makedirs(directory)
    except OSError:
        pass

    wav, sr = librosa.load(track, sr=sample_rate, mono=True)
    soundfile.write(file_dst, wav, sr)


def resample_mix_list(args):
    id_track, (mix, dst) = args

    print(f"process {id_track+1}")

    directory = path.join(dst, str(id_track + 1))
    name = str(id_track + 1)

    try:
        os.makedirs(directory)
    except OSError:
        pass

    blend_stems(mix, directory, name)


tracks = get_stems_with_annotation()
mix = generate_stems_combin(tracks)

"""
tracks_instru = sum(tracks_instru.values(), [])
tracks_voices = sum(tracks_voices.values(), [])

print(len(tracks), len(tracks_instru), len(tracks_voices))
csv2midi_naive(
    "/home/arthur/Documents/research/medleydb/medleydb/data/Annotations/Pitch/MusicDelta_Country1_STEM_05.csv"
)
exit(0)
"""
# print(tracks)
print(len(tracks))
# print(mix)
print(len(mix))

with mp.Pool(workers) as p:
    p.map(
        resample_mix_list, enumerate(zip(mix, [dst_path] * len(mix))),
    )
exit(0)

tracks_instru, tracks_voices = get_stems()

tracks_instru = sum(tracks_instru.values(), [])
tracks_voices = sum(tracks_voices.values(), [])

print(tracks_instru)
print(len(tracks_instru), len(tracks_voices))

with mp.Pool(workers) as p:
    p.map(
        resample_list, enumerate(zip(tracks_instru, [dst_path] * len(tracks_instru))),
    )
